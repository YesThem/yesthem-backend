const express = require('express')
const app = express()
const solutionRoutes = express.Router()
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Company = require('../models/company')
const Solution = require('../models/solution')
const Benefit = require('../models/benefit')
const atob = require('atob')
const moment = require('moment')
const multer = require('multer')
const _ = require('lodash')

const sanitizeString = (str) => {
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"").toLowerCase()
    str = str.split('.').join("")
    str = str.split(" ").join("")
    return str.trim()
}

const getPayload = (token) => {
  const payload = token.split('.')[1]
  return JSON.parse(atob(payload))
}

const multerConfig = {
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/static/images/uploads/')
        },

        filename: function (req, file, cb) {
            const ext = file ? file.mimetype.split('/')[1] : ''
            if (!req.body.solutionId && file) {
                cb(null, req.body.company + '-solution-' + req.body.index + '.' +ext)
            } else if (req.body.solutionId && !file) {
                cb(null, false)
            } else if (req.body.solutionId && file) {
                cb(null, req.body.company + '-solution-' + req.body.index + '.' +ext)
            } else {
                cb(null, false)
            }
            // cb(null, req.body.companyIdentifier + '-solution-' + req.body.solutionIndex + '.' +ext)
        },

        fileFilter: function(req, file, next){
            if(!file){
                next();
            } else {
                if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
                    cb(null, true)
                }else{
                    cb(new Error('Images should be either JPEG or PNG!'))
                }

                if ((file.size / 1024) < 1024) {
                    cb(null, true)
                }else{
                    cb(new Error('Images should be less than 1Mb!'))
                }
            }
            return next();
        }
    })
}

const multerConfigLinkedin = {
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/static/images/uploads/validate/')
        },

        filename: function (req, file, cb) {
            const ext = file ? file.mimetype.split('/')[1] : ''
            if (file) {
                cb(null, sanitizeString(req.body.id) + '-picture.' + ext)
            } else {
                cb(null, false)
            }
        },

        fileFilter: function(req, file, next){
            if(!file){
                next();
            } else {
                if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
                    cb(null, true)
                }else{
                    cb(new Error('Images should be either JPEG or PNG!'))
                }

                if ((file.size / 1024) < 1024) {
                    cb(null, true)
                }else{
                    cb(new Error('Images should be less than 1Mb!'))
                }
            }
            return next();
        }
    })
}

const auth_middleware = function(req, res, next){
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.params.token || req.get('Authorization').substring(7)

    if(token) {
        jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
            if(!err){
                User.findOne().where({ _id : getPayload(token)._id }).exec( (error, result) => {
                    if(!error){
                        next()
                    }else{
                        res.status(401).json({status: 401, info: "Unauthorized Access: "+err})
                    }
                })
            }else{
                res.status(401).json({status: 401, info: "Unauthorized Access: "+err})
            }
        })
    }else{
        res.status(403).json({ 
            success: false, 
            message: 'No token provided.'
        })
    }
}

const headers_middleware = function(req, res, next) {
    res.set('Cache-Control', 'public, max-age=0, must-revalidate, no-store')
    next()
}

app.use(headers_middleware)

solutionRoutes.route('/all').get(async(req, res) => {
    try {

        /*let company = await Company.findOne({ identifier: req.params.identifier }).deepPopulate('solutions solutions.benefits solutions.benefits.validations', {
                populate: {
                    'solutions.benefits.validations' : {
                        select: 'linkedin'
                    }
                }
            }).exec()*/

        let solutions = await Solution.find().where({active: true}).deepPopulate('company benefits benefits.validations', {
            populate: {
                'benefits.validations' : {
                    select: 'linkedin'
                }
            }
        }).sort('-_id').exec()
        res.status(200).json({ solutions: solutions })
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information - '+error })
    }
})

app.use(auth_middleware)

solutionRoutes.route('/benefits/validate').post(multer(multerConfigLinkedin).single('pictureUrl'), async(req, res) => {
    const isLoggedIn = typeof(req.body.loggedIn) !== 'undefined' //this could be taken as being a mark of wether the validation was with linkedin or not
    try {
        let benefits = await Benefit.find({ solution: req.body.solutionId }).exec()
        if (benefits) {
            for (let i = 0, len = benefits.length; i < len; i++) {

                let jBenefits = isLoggedIn === true ? req.body.benefits : JSON.parse(req.body.benefits)
                if ( jBenefits.some( benefit => benefit._id == benefits[i]._id) ) {

                    if ( !_.find(benefits[i].validations, function (o) { return o.id.toString() === (isLoggedIn === true ? req.body.linkedin.id.toString() : req.body.id) })) {
                        benefits[i].validations.push(isLoggedIn === true ? req.body.linkedin : { id: req.body.id, firstName: req.body.firstName, lastName: req.body.lastName, headline: req.body.headline, industry: req.body.industry, pictureUrl: '/static/images/uploads/validate/' + sanitizeString(req.body.id) + '-picture.' + req.file.mimetype.split('/')[1], positions: req.body.positions })
                        await benefits[i].save()
                    }
                } else {
                    benefits[i].validations = benefits[i].validations.filter( validation => validation.id.toString() !== (isLoggedIn === true ? req.body.linkedin.id.toString() : req.body.id) )
                    await benefits[i].save()
                }
            }
            res.status(200).json({ message: 'Benefits validated. Thank you for your input.' })
        } else {
            res.status(400).json({ message: 'Unable to save data. Reason: Solution doesn\'t exist'})
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error validating your changes - '+error})
    }
})

solutionRoutes.route('/benefits/register').post( async(req, res) => {
    try {
        let company = await Company.findOne({ _id: req.body.company }).exec()

        if (!company) {
            res.status(400).json({ message: 'Unable to save data. Reason: Company doesn\'t exist'})
        } else {
            let solution = await Solution.findOne().where({ _id: req.body.solution, company: req.body.company }).exec()
            if (!solution) {
                res.status(400).json({ message: 'Unable to save data. Reason: Solution doesn\'t exist'})
            } else {
                let iBenefits = req.body.benefits
                for (let i = 0, len = iBenefits.length; i < len; i++) {
                    if (iBenefits[i]._id) {
                        let benefit = await Benefit.findOne({ _id: iBenefits[i]._id, solution: solution._id }).exec()
                            benefit.description = iBenefits[i].description
                            await benefit.save()
                    } else {
                        let benefit = new Benefit()
                            benefit.solution = req.body.solution
                            benefit.description = iBenefits[i].description
                            benefit.validations = []
                            benefit.active = true
                            await benefit.save()

                            solution.benefits.push(benefit._id)
                            await solution.save()
                    }
                }
                res.status(200).json({ message: 'Benefits saved successfully.' })
            }
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error registering your information - '+error})
    }
})

solutionRoutes.route('/remove').post(async(req, res) => {
    try {
        await Solution.findOneAndRemove({ _id: req.body._id, company: req.body.company }).exec()
        res.status(200).json({ message: 'Solution removed successfully.' })
    } catch (error) {
        res.status(400).json({ message: 'There was an error removing this solution - '+error})
    }
})

solutionRoutes.route('/register').post(multer(multerConfig).single('logo'), (req, res) => {
    const solutionSchema = Joi.object().keys({
        // title: Joi.string().min(3).max(30).required(),
        description: Joi.string().min(5).max(53).required(),
        specialization: Joi.string().required(),
        category: Joi.string().required(),
        happyNumber: Joi.string().allow('')
        // customCategory: Joi.string().min(3).max(50).required(),
    })

    const result = Joi.validate({
        // title: req.body.title,
        specialization: req.body.specialization,
        category: req.body.category,
        // customCategory: req.body.customCategory,
        description: req.body.description,
        happyNumber: req.body.happyNumber

    }, solutionSchema, { abortEarly: false, language: {
        any: {
            required: '!! {{label}} is required.'
        },
        string: {
            min: '!! {{label}} must be at least {{limit}} characters long.',
            regex: {
                base: "!! {{label}} must contain letters and numbers and can't contain special characters."
            }
        }
    }}, async (joiErr, value ) => {
        if(joiErr == null) {
            try {
                let company = await Company.findOne({ _id: req.body.company }).exec()

                if (!company) {
                    res.status(400).json({ message: 'Unable to save data. Reason: Company doesn\'t exist'})
                } else {
                    let solution = null
                    let isNew = false

                    if (req.body.solution !== '') { // req.body.solution is the id
                        solution = await Solution.findOne({ _id: req.body.solution }).exec()
                        if (!solution) {
                            res.status(400).json({ message: 'Unable to save data. Reason: Trying to edit a solution that doesn\'t exist'})    
                        } else {
                            isNew = false
                        }
                    } else {
                        solution = new Solution ()
                        isNew = true
                    }

                    const ext = req.file ? req.file.mimetype.split('/')[1] : ''
                    solution.index = req.body.index // isNew? company.solutions.length + 1 : solution.index
                    solution.company = company._id
                    solution.industry = req.body.specialization
                    solution.category = req.body.category
                    solution.description = req.body.description
                    solution.happyNumber = req.body.happyNumber
                    // solution.title = req.body.title
                    if (isNew && req.file) {
                        solution.image = company._id + '-solution-' + (req.body.index) + '.' +ext
                    } else if (!isNew && !req.file) {
                        solution.image = solution.image
                    } else if (!isNew && req.file) {
                        solution.image = company._id + '-solution-' + (req.body.index) + '.' +ext
                    } else {
                        throw("Missing picture.")
                    }
                    // solution.image = isNew? company.identifier + '-solution-' + (company.solutions.length + 1) + '.' +ext : solution.image
                    // solution.benefits = solution.benefits? solution.benefits : []
                    solution.active = true
                    solution.validations = solution.validations ? solution.validations : []
                    await solution.save()

                    // After you save the solution, you save the benefits, this way wether it's a new or an old solution, you always have a solution id to look for.
                    let benefits = await Benefit.find({ solution: solution._id }).exec()
                    let iBenefits = JSON.parse(req.body.results) // req.body.results

                    for (let i = 0, len = iBenefits.length; i < len; i++) { // edit and add new ones
                        if ( benefits.some( benefit => benefit._id == iBenefits[i]._id) ) {
                            let edit = await Benefit.findOne({ _id: iBenefits[i]._id, solution: solution._id }).exec()
                                edit.description = iBenefits[i].description
                                await edit.save()
                        } else {
                            let benefit = new Benefit()
                                benefit.solution = solution._id
                                benefit.description = iBenefits[i].description
                                benefit.validations = []
                                benefit.active = true
                                await benefit.save()

                                solution.benefits.push(benefit._id)
                        }
                    }

                    for (let i = 0, len = benefits.length; i < len; i++) { // delete the ones not received
                        if (iBenefits.some( benefit => benefit._id == benefits[i]._id) == false) {
                            await Benefit.findOneAndRemove({ _id: benefits[i]._id, solution: solution._id }).exec()
                            solution.benefits.splice(i, 1)
                        }
                    }

                    await solution.save()



                    /*

                    for (let i = 0, len = benefits.length; i < len; i++) {

                        if ( req.body.benefits.some( benefit => benefit._id == benefits[i]._id) ) {

                            if ( !_.find(benefits[i].validations, function (o) { return o.id.toString() === req.body.linkedin.id.toString() })) {
                                benefits[i].validations.push(req.body.linkedin)
                                await benefits[i].save()
                            }
                        } else {
                            benefits[i].validations = benefits[i].validations.filter( validation => validation.id.toString() !== req.body.linkedin.id.toString() )
                            await benefits[i].save()
                        }
                    }
                      

                    */

                    /*for (let i = 0, len = iBenefits.length; i < len; i++) {
                        if (iBenefits[i]._id) {
                            let benefit = await Benefit.findOne({ _id: iBenefits[i]._id, solution: solution._id }).exec()
                                benefit.description = iBenefits[i].description
                                await benefit.save()
                        } else {
                            let benefit = new Benefit()
                                benefit.solution = solution._id
                                benefit.description = iBenefits[i].description
                                benefit.validations = []
                                benefit.active = true
                                await benefit.save()

                                solution.benefits.push(benefit._id)
                        }
                    }
                    await solution.save()*/

                    if (isNew) {
                        company.solutions.push(solution)
                    }
                    await company.save()

                    res.status(200).json({ message: 'Solution saved successfully.' })
                }
            } catch (error) {
                res.status(400).json({ message: 'There was an error registering your information - '+error})
            }
        }else {
            let errors = []
            for (var err in joiErr.details){
                errors.push(joiErr.details[err].message)
            }
            res.status(400).json({ message: errors.toString() })
        }
    })
})

module.exports = solutionRoutes;