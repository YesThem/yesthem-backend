const express = require('express')
const app = express()
const userRoutes = express.Router()
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Company = require('../models/company')
const atob = require('atob')
const moment = require('moment')
const multer = require('multer')
const nodemailer = require('nodemailer')

const sanitizeString = (str) => {
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"").toLowerCase()
    str = str.split('.').join("")
    str = str.split(',').join("")
    str = str.split(';').join("")
    str = str.split(':').join("")
    str = str.split('?').join("")
    str = str.split('!').join("")
    str = str.split('|').join("")
    str = str.split('¡').join("")
    str = str.split('¿').join("")
    str = str.split(" ").join("")
    return str.trim()
}

const getPayload = (token) => {
  const payload = token.split('.')[1]
  return JSON.parse(atob(payload))
}

const multerConfig = {
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/static/images/uploads/')
        },

        filename: function (req, file, cb) {
            const ext = file.mimetype.split('/')[1]
            cb(null, sanitizeString(req.body.companyName)+'-logo.'+ext)
        },

        fileFilter: function(req, file, next){
            if(!file){
                next();
            } else {
                if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
                    cb(null, true)
                }else{
                    cb(new Error('Images should be either JPEG or PNG!'))
                }

                if ((file.size / 1024) < 1024) {
                    cb(null, true)
                }else{
                    console.log('multer error: error uploading image')
                    cb(new Error('Images should be less than 1Mb!'))
                }
            }
            return next();
        }
    })
}

const transporter = nodemailer.createTransport({
  service: process.env.EMAIL_SERVICE,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASSWORD
  }
})

const composeEmail = function (email, name) {
    const link = 'https://www.yesthem.com/password-change/token='+jwt.sign({ email: email, name: name }, process.env.JWT_SECRET)

    const mailOptions = {
      from: 'pruebaparapartnergood@gmail.com',
      to: email,
      subject: 'Reinicio de la contraseña de tu cuenta Yesthem',
      html: 'Hola '+name+',<br><br>Este correo se te envía porque solicitaste un cambio de la contraseña de tu cuente Yesthem. <br><br>Para cambiar tu contraseña sigue el link de la parte inferior. Si no hiciste esta solicitud, ignora este e-mail.<br><br>'+link+'<br><br>Yesthem Admin.'
    }

    return mailOptions
}

const auth_middleware = function(req, res, next){
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.params.token || req.get('Authorization').substring(7)

    if(token) {
        jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
            if(!err){
                User.findOne().where({ _id : getPayload(token)._id }).exec( (error, result) => {
                    if(!error){
                        next()
                    }else{
                        res.status(401).json({status: 401, info: "Unauthorized Access: "+err})
                    }
                })
            }else{
                res.status(401).json({status: 401, info: "Unauthorized Access: "+err})
            }
        })
    }else{
        res.status(403).json({ 
            success: false, 
            message: 'No token provided.'
        })
    }
}

const headers_middleware = function(req, res, next) {
    res.set('Cache-Control', 'public, max-age=0, must-revalidate, no-store')
    next()
}

userRoutes.route('/register').post((req, res) => {
    const userSchema = Joi.object().keys({
        name: Joi.string().regex(/^[^0-9]+$/).min(3).max(60).required(),
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9\+\-\!\¡\%]+$/).required(),
        companyName: Joi.string().min(3).max(30).required(),
        industry: Joi.string().required(),
        description: Joi.string().min(3).max(100).required()
    })

    const result = Joi.validate({
        name : req.body.name, 
        email: req.body.email, 
        password: req.body.password,
        companyName: req.body.commercialName,
        industry: req.body.specialization,
        description: req.body.description

    }, userSchema, { abortEarly: false, language: {
        any: {
            required: '!! {{label}} is required.'
        },
        string: {
            min: '!! {{label}} must be at least {{limit}} characters long.',
            regex: {
                base: "!! {{label}} must contain letters and numbers and can't contain special characters."
            }
        }
    }}, async (joiErr, value ) => {
        if(joiErr == null) {

            try {

                let checkCompany = await Company.findOne({name: req.body.commercialName}).exec()

                if (!checkCompany) {
                    let user = new User ()
                        user.registered = moment().utc()
                        user.name  = req.body.name
                        user.email = req.body.email
                        user.setPassword(req.body.password)
                        user.active = true
                        user.role = 'admin'
                        user.companyIdentifier = sanitizeString(req.body.commercialName)
                    await user.save()

                    let company = new Company ()
                        company.representative = user._id
                        company.name = req.body.commercialName
                        company.description = req.body.description
                        company.industry = req.body.specialization
                        company.location = ''
                        company.logo = null
                        company.coverImage = null
                        company.howToSay = '1'
                        company.customHowToSay = ''
                        company.facebook = ''
                        company.linkedin = ''
                        company.twitter = ''
                        company.youtube = ''
                        company.thanks = ''
                        company.active = true
                        company.published = false
                        company.solutions = []
                        company.identifier = user.companyIdentifier
                    await company.save()

                    console.log(company._id)
                    user.company = company._id
                    await user.save()

                    const token = user.generateJwt()

                    res.status(200).json({ token: token, message: 'Your account was successfully created.' })
                } else {
                    res.status(400).json({ message: 'Esa Compañia ya está registrada en nuestro sistema.'})
                }
            } catch (error) {
                res.status(400).json({ message: 'There was an error registering your information - '+error})
            }
        }else {
            let errors = []
            for (var err in joiErr.details){
                errors.push(joiErr.details[err].message)
            }
            res.status(400).json({ message: errors.toString() })
        }
    })
})

userRoutes.route('/verify').post( async(req, res) => {
    console.log(req.body.user, req.body.linkedin)
    try {
        let user = await User.findOne({ _id: req.body.user }).exec()
            user.linkedin.firstName = req.body.linkedin.firstName
            user.linkedin.lastName = req.body.linkedin.lastName
            user.linkedin.headline = req.body.linkedin.headline
            user.linkedin.industry = req.body.linkedin.industry
            user.linkedin.pictureUrl = req.body.linkedin.pictureUrl
            user.linkedin.position = req.body.linkedin.positions.values[0].company.name
            user.verified = true
        await user.save()

        console.log('user', user)

        const token = user.generateJwt()
        console.log('token', token)

        res.status(200).json({ token: token, message: 'Your account was successfully verified.' })
    } catch (error) {
        res.status(400).json({ message: 'There was an error verifying your account - '+error})
    }
})

userRoutes.route('/login').post( (req, res) => {
    const userSchema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9\+\-\!\¡\%]+$/).required()
    })

    Joi.validate({
        email: req.body.email, 
        password: req.body.password

    }, userSchema, { abortEarly: false, language: {
        any: {
            required: '!! {{label}} is required.'
        },
        string: {
            min: '!! {{label}} must be at least {{limit}} characters long.',
            regex: {
                base: "!! {{label}} must contain letters and numbers and can't contain special characters."
            }
        }
    }}, async (joiErr, value ) => {
        if(joiErr == null) {
            try {
                let user = await User.findOne({ email: req.body.email, active: true }).exec()

                if (!user) {
                    res.status(400).json({ message: 'There is no account registered with that e-mail.'})
                } else {
                    if(user.validPassword (req.body.password)) {
                        const token = user.generateJwt()
                        res.status(200).json({ message: 'Login successful.', token: token })
                    } else {
                        res.status(400).json({ message: 'Invalid password.'})
                    }
                }

            } catch (error) {
                res.status(400).json({ message: 'There was an error logging you in - '+error})
            }
        }else {
            let errors = []
            for (var err in joiErr.details){
                errors.push(joiErr.details[err].message)
            }
            res.status(400).json({ message: 'Incorrect credentials.'})
        }
    })
})

userRoutes.route('/password/reset').post((req, res) => {
    const emailSchema = Joi.object().keys({
        email: Joi.string().email().required()
    })

    Joi.validate({ email : req.body.email }, emailSchema, { abortEarly: false, language: {
        any: {
            required: '!! {{label}} es requerido.'
        },
        string: {
            min: '!! {{label}} must be at least {{limit}} characters long.',
            regex: {
                base: "!! {{label}} must contain letters and numbers and can't contain special characters."
            }
        }
    }},  async (joiErr, value ) => {
            if(joiErr === null) {
                try {
                    let user = await User.findOne({email : req.body.email}).exec()
                    if (user) {
                        transporter.sendMail(composeEmail(user.email, user.name), (error, info) => {
                          if (error) {
                            res.status(400).json({message: "No se pudo enviar el e-mail para cambiar la contraseña. Por favor intentelo de nuevo más tarde."})
                          } else {
                            res.status(200).json({message: "Se envió un e-mail a la dirección que proporcionaste. Por favor sigue las instrucciones allí para cambiar tu contraseña. Ahora puedes cerrar esta pestaña."})
                          }
                        })
                    } else {
                        res.status(400).json({message: "No hay cuenta registrada con ese e-mail."})
                    }
                } catch (error) {
                    console.log(error)
                    res.status(400).json({message: "Hubo un error cambiando tu contraseña. Por favor intentalo de nuevo más tarde."})
                }
            }else {
                let errors = []
                for (var err in joiErr.details){
                    errors.push(joiErr.details[err].message)
                }
                res.status(400).send({message: errors.toString()})
            }
    })
})

userRoutes.route('/password/change').post((req, res) => {
    const passwordSchema = Joi.object().keys({
        password: Joi.string().regex(/^[a-zA-Z0-9\+\-\!\¡\%]+$/).required()
    })

    jwt.verify(req.body.items.token, process.env.JWT_SECRET, (err, decoded) => {
        if(err){
            res.status(403).json({status: 403, message: "Acceso no autorizado."})
        }else{
            Joi.validate({ password : req.body.items.password }, passwordSchema,{ abortEarly: false, language: {
                any: {
                    required: '!! {{label}} es requerido.'
                },
                string: {
                    min: '!! {{label}} must be at least {{limit}} characters long.',
                    regex: {
                        base: "!! {{label}} debe contener letras y numeros y no puede contener caracteres especiales."
                    }
                }
            }}, async (joiErr, value ) => {
                if(joiErr == null) {
                    try {
                        let user = await User.findOne({name: decoded.name, email : decoded.email}).exec()
                        if (user) {
                            user.setPassword(req.body.items.password)
                            await user.save()
                            res.status(200).json({message: "Contraseña cambiada con éxito."})
                        } else {
                            res.status(403).json({message: "Hubo un error reseteando tu contraseña. Por favor intentalo de nuevo más tarde."})
                        }
                    } catch (error) {
                        res.status(400).json({message: "Hubo un error cambiando tu contraseña. Por favor intentalo de nuevo más tarde."})
                    }
                }else {
                    let errors = []
                    for (var err in joiErr.details){
                        errors.push(joiErr.details[err].message)
                    }
                    res.status(400).send({message: errors.toString()})
                }
            })
        }
    })
})

app.use(headers_middleware)

userRoutes.route('/reload').post( async(req, res) => {
    try {
        let user = await User.findOne({ _id: req.body._id }).exec()

        if (!user) {
            res.status(400).json({ message: 'Use doesn\'t exist.'})
        } else {
            const token = user.generateJwt()
            res.status(200).json({ message: 'Reload successful.', token: token })
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error when reloading the user - '+error})
    }
})

module.exports = userRoutes;